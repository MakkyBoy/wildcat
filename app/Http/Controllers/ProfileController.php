<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    public function profile(){
    	return view('profiles.profile');
    }

    public function addProfile(Request $request){
    	//return $request->input('name');
    	$this->validate($request, [
    		'name' => 'required',
    		'designation' => 'required',
    		'profile_pic' => 'required'
    	]);
    	//return 'validation Passed';
    	$profiles = new Profile;
    	$profiles->name = $request->input('name');
    	$profiles->user_id = Auth::user()->id;
    	$profiles->designation = $request->input('designation');
    	if(Input::hasFile('profile_pic')){
$file = Input::file('profile_pic');
$file->move(public_path().'/uploads/', $file->getClientOriginalName());
$url = URL::to("/") . '/uploads/' . $file->getClientOriginalName();
    		
    		//return $url;
    		//exit(); (this spits outs the url of the image)
    	}
    	$profiles->profile_pic = $url; //$request->input('profile_pic');
    	$profiles->save();
    	return redirect('/home')->with('response', 'Profile Addedd Successfully');
    	//this spits with the response as successful.
    	//return Auth::user();
    	//exit();
    }
}
