<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Category;
use App\Post;
use Auth;

class PostController extends Controller
{
    public function post(){
    	$categories = category::all();
    	return view('posts.post', ['categories' => $categories]);
    	//return $category; 
    	//exit();
    	//used to add category to the DB
    	
    }

    public function addPost(Request $request){
    	//return $request->post_title;
    	//or return $request->input('post_title');
    	/* used to test the title and category posted to the browser*/
    	$this->validate($request, [
    		'post_title' =>'required',
    		'post_body' => 'required',
    		'category_id' => 'required',
    		'post_image' => 'required'

    	 ]);
		//return 'Validation passed'; testing server side validation
		$posts = new Post;
    	$posts->post_title = $request->input('post_title');
    	$posts->user_id = Auth::user()->id;
    	$posts->post_body = $request->input('post_body');
    	$posts->category_id = $request->input('category_id'); 
    	   
    	if(Input::hasFile('post_image')){
$file = Input::file('post_image');
$file->move(public_path().'/uploads/', $file->getClientOriginalName());
$url = URL::to("/") . '/uploads/' . $file->getClientOriginalName();
    	}
    	$posts->post_image = $url; //$request->input('profile_pic');
    	$posts->save();
    	return redirect('/home')->with('response', 'Blog Successfully Published');
    }

    public function view($post_id){
        //return $post_id;
        $posts = Post::where('id', '=', $post_id)->get();
        $categories = Category::all();
        //return $categories; returns all the categories on the view in JSON format
        //exit();
        return view('posts.view', ['posts' => $posts, 'categories' => $categories]);
        //Now return view as an array which is inside the post folder
        //return $post; returns all the post on the view in JSON format
        //exit();
    }

    public function edit($post_id){
        $categories = Category::all();
        $posts = Post::find($post_id); //gives details of the particular post using its id
        $category = Category::find($posts->category_id);
        //return $posts; 
        //exit();
        //return $post_id;
        return view('posts.edit', ['categories' => $categories, 'posts' => $posts, 'category' => $category]);
    }
}
