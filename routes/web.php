<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//routes for authentication
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//routes for Home page
Route::get('/home', 'HomeController@index')->name('home');
//routes for Home page
Route::get('/about', 'AboutController@about')->name('about');
//routes for Add Post page
Route::get('/post', 'PostController@post')->name('post');
//routes for Add Category page
Route::get('/category', 'CategoryController@category')->name('category');
//routes for Add Profile page
Route::get('/profile', 'ProfileController@profile')->name('profile');
//routes for Add Category page
Route::post('/addCategory', 'CategoryController@addCategory')->name('addCategory');
//route for Add Profile 
Route::post('/addProfile', 'ProfileController@addProfile');
//route for Add Post
Route::post('/addPost', 'PostController@addPost');
//routes for View Post
Route::get('/view/{id}', 'PostController@view');
//routes for Edit Post
Route::get('/edit/{id}', 'PostController@edit');